precision highp float;

attribute float pindex;
attribute vec3 position;
attribute vec3 offset;
attribute vec2 uv;
attribute float angle;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

uniform float uTime;
uniform float uRandom;
uniform float uDepth;
uniform float uSize;
uniform vec2 uTextureSize;
uniform sampler2D uTexture;
uniform sampler2D uTouch;

varying vec2 vPUv;
varying vec2 vUv;

#pragma glslify: snoise2 = require(glsl-noise/simplex/2d)

float random(float n) {
	return fract(sin(n) * 43758.5453123);
}

void main() {
	vUv = uv;

	// particle uv
	vec2 puv = offset.xy / uTextureSize;
	vPUv = puv;

	// pixel color
	vec4 colA = texture2D(uTexture, puv);
	float grey = colA.r * 0.01 + colA.g * 0.01 + colA.b * 0.01;

	// displacement
	vec3 displaced = offset;
	// randomise
	//displaced.xy += vec2(random(pindex) - 0.5, random(offset.x + pindex) - 0.5) * uRandom;
	//displaced.z += rndz * (random(pindex) * 2.0 * uDepth);

	float rndz = (random(pindex) + snoise2(vec2(pindex * 0.1, uTime * 0.1)));
	// center
	displaced.xy -= uTextureSize * 0.5;

	// touch
	//力の大きさを追加
	// テクスチャから読みだした値（vec4 destColor）は……
    // XY が頂点の座標を、ZW で頂点の進行方向ベクトルを表している
	vec4 t = texture2D(uTouch, puv);
	vec2 v = normalize(t.xy - colA.xy) * 0.2; // カーソル位置
    vec2 w = normalize(v + colA.zw);
	float velocity = 0.0;          
	if(t.x > colA .x && t.y > colA.y) {
	velocity += (t.x - colA.x) * (t.y - colA.y) ;
	}else {
	velocity += (colA.x - t.x) * (colA.y - t.y) ;

	}
    
    vec4 destColor = vec4(colA.xy + w * 50.0 * velocity, w);
	displaced.z += t.z * 20.0 * rndz;
	displaced.x += cos(angle) * t.x * 20.0 * rndz * destColor.x;
	displaced.y += sin(angle) * t.y * 20.0 * rndz * destColor.y;

	// particle size
	float psize = (snoise2(vec2(uTime, pindex) * 0.5) + 2.0);
	psize *= max(grey, 0.2);
	psize *= uSize;

	// final position
	vec4 mvPosition = modelViewMatrix * vec4(displaced, 1.0);
	mvPosition.xyz += position * psize;
	vec4 finalPosition = projectionMatrix * mvPosition;

	gl_Position = finalPosition;
}
