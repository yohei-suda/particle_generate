# Particle generator with Three.js

## Run
- Install `npm install`
- Run `npm start`
- Build `npm run build`

## Libraries
- [ControlKit](https://github.com/brunoimbrizi/controlkit.js) - GUI ※default not usable
- [gsap](https://www.npmjs.com/package/gsap) - animation platform
- [glslify](https://github.com/glslify/glslify) - module system for GLSL
- [stats.js](https://github.com/mrdoob/stats.js/) - performance monitor
- [Three.js](https://github.com/mrdoob/three.js/) - WebGL library
